/*RAMA PROPIA ROBER MASTER EVO_1*/

/*

ESTA ES LA RAMA MASTER

 2- Carga un array  de tipo int, cas notas no módulo de programación ,dos 30 alumnos da clase de DAM .
•	 Visualiza o numero de aprobados e o de suspensos  .
•	 Calcula e visualiza a nota media da clase
•        Visualiza a nota mais alta .

 */

package boletin17ex2;

import javax.swing.JOptionPane;

/**
 *
 * @author pcelardperez
 * @since 21/1/14
 * @version master
 */
public class Aplicacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        int not[]= new int[6];
        Datos ver = new Datos();
        
        ver.notas(not);             //llamamos al metodo para ver las notas
        ver.SuspensoAprobado(not);  //llamamos al metodo para ver los aprobados y suspensos
        //ver.notaMedia(not);         //llamamos al metodo para ver la nota media
        //ver.notaAlta(not);          //llamamos al metodo para ver la nota mas alta
        JOptionPane.showMessageDialog(null, "Nota más alta: "+ver.notaAlta(not)+ ver.notaMedia(not));
    }
    
}
