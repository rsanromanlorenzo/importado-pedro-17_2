/*
 Clase con notas de la rama MASTER
 */

package boletin17ex2;

/**
 *
 * @author pcelardperez
 * @version master
 */
public class Datos {
    
    //Array para notas
    int notas[] = new int[6];
        
    /**
     * Le damos valores al array notas
     * @param not 
     */
    public void notas(int not[]){
        for (int i=0; i<not.length; i++){     //Este bucle se repetira 6 veces, 1 para cada alumno y nota                           
            not[i]=(int)(Math.random()*10 + 1);     //Le da valores aleatorios.
            System.out.println("Alumno "+(i+1)+" = "+not[i]);    //muestra por pantalla las notas
        }  
    }
    
    /**
     * comprobamos si esta suspenso o aprobado y contamos los totales
     * @param not 
     */
    public void SuspensoAprobado(int not[]){
        int aprobado=0;  //Contador para aprobados.
        int suspenso=0;  //Cotador para suspensos.
        for (int x=0; x<not.length;x++){
            
            if(not[x]>=5){
                aprobado++;  //Sumar 1 a aprobados.
                
            }else{
                suspenso++;  //Sumar 1 a suspensos.
            }
            }
            System.out.println("Suspensos: "+suspenso);
            System.out.println("Aprobados: "+aprobado);
        
    }
    
    /**
     * Calculamos la nota media
     * @param not 
     */
    public int notaMedia(int not[]){
        int aux=0;  //Auxiliar para sumar notas
        int media=0;//La nota media(suma/6(total))
        for(int i=0; i<not.length; i++){
            aux=aux+not[i];   //sumamos las notas
            media=aux/6;      //calculamos la media
            
        }
        return media;
        //System.out.println("Nota media: "+media);
    }
    
    public int notaAlta(int not[]){
        int auxalta=0; //auxiliar para la nota mas alta
        int auxbaja=10;//auxiliar para la nota mas baja, se inicializa en 10 porque is nosiempre seria 0
        for(int i=0;i<not.length;i++){
            if(not[i]>auxalta){  //Si la nota es más alta que el auxiliar sustituye a la nota mas alta hasta el momento
                auxalta=not[i];
            }else if(not[i]<auxbaja){  //Si no sustituye a la nota mas baja
                auxbaja=not[i];
            }
        }
        return auxalta;
        //System.out.println("Nota más alta: "+auxalta+"\nNota más baja: "+auxbaja);
    }
}
